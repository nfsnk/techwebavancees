# Technologies Web Avancées

## Authors
Coly Francois Xavier et Sonko Ndeye Fatou. 

## Lien d'une video demonstrative
[ ] [Video Youtube](https://youtu.be/Mz1lUobiwRE) 

## Lien pour le schéma de notre base de données
[ ] [Lien Figma](https://www.figma.com/file/dshexs49AbZmEnbBI53yaR/TechWeb?node-id=0%3A1) 

## Name
Scolarité Manager

## Description
L’objectif de ce projet est de réaliser une application web qui permet la gestion des notes des 
étudiants dans un service de scolarité. 
L'application devrait normalement permettre de réaliser les actions suivantes :
1- Inscrire un étudiant dans une spécialité à travers un formulaire permettant de 
renseigner le nom de l’étudiant, son prénom et sa spécialité. 
2- Saisir la note d’un étudiant à travers un formulaire permettant de renseigner le nom 
de l’étudiant, son prénom, l’intitulé du module et finalement la note.
3- Afficher la liste des étudiants par spécialité.
4- Afficher les notes des étudiants dans un module bien précis
Mais on n'a pas pu la finir à temps. Et on a rencontré quelques difficultés avec la connexion entre le back et la base de données.

## Test
Pour le test, il est nécessaire d'inserer les tuples suivants dans la table Module pour assurer la bonne marche des services:
insert into MODULE(NOM_MODULE) values
    ('Technologies web'),
    ('Langage C'),
    ('Programmation fonctionnelle'),
    ('Programmation logique'),
    ('Programmation système'),
    ('Anglais'),
    ('Espagnol'),
    ('Allemand'),
    ('Projet sécurité'),
    ('Réseau');

## Project status
Le projet n'a pas abouti à sa fin. On y travaillera quand on aura un peu de temps.
