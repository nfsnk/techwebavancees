$(document).ready(()=>{
    //Utilisation des datatables
    $('#table_id').DataTable();

    //Constantes
    const module= $("#module");
    const bodyTab=$("tbody");
    let listeNotes;

    $.get("https://localhost:8080/api/notes",(resp)=>{
        listeNotes=resp;
    });

    module.change(()=>{
        const moduleChoisi=module.val();
        $.get("https://localhost:8080/api/notes/"+moduleChoisi+"",(resp)=>{
            listeNotes=resp;
        });
        for (var i=0;i<listeNotes.length;i++){
            bodyTab.append("<tr>" +
                "<td>"+listeNotes[i][0]+"</td>" +
                "<td>"+listeNotes[i][1]+"</td>" +
                "<td>"+listeNotes[i][2]+"</td>" +
                "<td>"+listeNotes[i][3]+"</td>" +
                "<td>"+listeNotes[i][4]+"</td>" +
                "</tr>");
        }

    });
});