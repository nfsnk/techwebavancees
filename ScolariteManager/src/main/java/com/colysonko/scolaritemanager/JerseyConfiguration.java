package com.colysonko.scolaritemanager;

import com.colysonko.scolaritemanager.etudiant.EtudiantRessources;
import com.colysonko.scolaritemanager.module.ModuleRessources;
import com.colysonko.scolaritemanager.note.NoteRessources;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.servlet.ServletProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

import javax.ws.rs.ApplicationPath;

@Component
@ApplicationPath("api")
@Configuration
public class JerseyConfiguration extends ResourceConfig {
    public JerseyConfiguration() {
        register(NoteRessources.class);
        register(ModuleRessources.class);
        register(EtudiantRessources.class);
        property(ServletProperties.FILTER_FORWARD_ON_404, true);
    }
}