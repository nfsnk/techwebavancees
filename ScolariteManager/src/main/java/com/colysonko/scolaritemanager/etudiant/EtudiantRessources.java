package com.colysonko.scolaritemanager.etudiant;

import org.springframework.beans.factory.annotation.Autowired;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.ArrayList;
import java.util.Optional;

@Path("etudiants")
public class EtudiantRessources {
    @Autowired
    private EtudiantRepository etudiantRepository;

    //Creer un etudiant
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Etudiant createEtudiant(Etudiant e) {
        return (etudiantRepository.save(e));
    }

    //Read tous les etudiants
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<Etudiant> getAllEtudiant() {
        return new ArrayList<>(etudiantRepository.findAll());
    }


    //Read letudiant dont l'id est entré
    @GET
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getEtudiantById(@PathParam("id") Long id) {
        Optional<Etudiant> e = etudiantRepository.findById(id);
        if (e.isPresent()){
            return Response.ok(e.get()).build();
        } else {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
    }

    //Read la liste des etudiants qui sont dans la specialite precisee
    @GET
    @Path("etudiants/{module}")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Etudiant> getEudiantByModule(@PathParam("module") String module) {
        return etudiantRepository.findEtudiantsByModule(module);
    }


    //Delete l'etudiant dont l'id est entre
    @DELETE
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response deleteEtudiantById(@PathParam("id") Long id){
        if (etudiantRepository.findById(id).isPresent()) {
            try {
                etudiantRepository.deleteById(id);
            } catch (Exception e) {
                return Response.serverError().build();
            }
        }
        return Response.noContent().build();
    }


    //Update ladresse dont l'id est entre
    @PATCH
    @Path("{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response updateAdresse(@PathParam("id") Long id, Etudiant e){
        String adresse = e.getAdresseEtudiant();
        Optional<Etudiant> optional = etudiantRepository.findById(id);

        if (optional.isPresent()) {
        Etudiant etudiant = optional.get();
        etudiant.setAdresseEtudiant(adresse);
        etudiantRepository.save(etudiant);
        return Response.ok(etudiant).build();
        } else {
        return Response.status(Response.Status.NOT_FOUND).build();
        }
    }

    //Update totalement un etudiant
    @PUT
    @Path("{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Etudiant updateTotallyEtudiant(@PathParam("id") Long id, Etudiant e) {
            e.setIdEtudiant(id);
            return etudiantRepository.save(e);
    }

}
