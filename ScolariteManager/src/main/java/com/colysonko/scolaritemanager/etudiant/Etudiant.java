package com.colysonko.scolaritemanager.etudiant;

import com.colysonko.scolaritemanager.module.Module;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "etudiant")
public class Etudiant implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_Etudiant", nullable = false)
    private Long idEtudiant;

    @Column(name="prenom_Etudiant", nullable = false)
    private String prenomEtudiant;

    @Column(name="nom_Etudiant", nullable = false)
    private String nomEtudiant;

    @Column(name="specialite_Etudiant", nullable = false)
    private String specialiteEtudiant;

    @Column(name="mail_Personnel", nullable = false, unique = true)
    private String mailPersonnel;

    @Column(name="mail_Etudiant", nullable = false, unique = true)
    private String mailEtudiant;

    @Column(name="adresse_Etudiant", nullable = false)
    private String adresseEtudiant;

    @Column(name="nationalite_Etudiant", nullable = false)
    private String nationaliteEtudiant;

    @Column(name="telephone_Etudiant", nullable = false)
    private int telephoneEtudiant;

    @Column(name="ine_Etudiant", nullable = false)
    private String ineEtudiant;

    @ManyToMany
    @JoinTable(name = "module_Etudiant",
            joinColumns =@JoinColumn(name ="id_Etudiant"),
            inverseJoinColumns =  @JoinColumn(name = "id_Module"))
    private List<Module> module=new ArrayList<>();



    public Etudiant() {
        super();
    }

    public Etudiant(String prenomEtudiant, String nomEtudiant, String specialiteEtudiant, String mailPersonnel, String mailEtudiant, String adresseEtudiant, String nationaliteEtudiant, int telephoneEtudiant, String ineEtudiant, List<Module> module) {
        this.prenomEtudiant = prenomEtudiant;
        this.nomEtudiant = nomEtudiant;
        this.specialiteEtudiant = specialiteEtudiant;
        this.mailPersonnel = mailPersonnel;
        this.mailEtudiant = mailEtudiant;
        this.adresseEtudiant = adresseEtudiant;
        this.nationaliteEtudiant = nationaliteEtudiant;
        this.telephoneEtudiant = telephoneEtudiant;
        this.ineEtudiant = ineEtudiant;
        this.module = module;
    }

    public Long getIdEtudiant() {
        return idEtudiant;
    }

    public void setIdEtudiant(Long idEtudiant) {
        this.idEtudiant = idEtudiant;
    }

    public String getPrenomEtudiant() {
        return prenomEtudiant;
    }

    public void setPrenomEtudiant(String prenomEtudiant) {
        this.prenomEtudiant = prenomEtudiant;
    }

    public String getNomEtudiant() {
        return nomEtudiant;
    }

    public void setNomEtudiant(String nomEtudiant) {
        this.nomEtudiant = nomEtudiant;
    }

    public String getSpecialiteEtudiant() {
        return specialiteEtudiant;
    }

    public void setSpecialiteEtudiant(String specialiteEtudiant) {
        this.specialiteEtudiant = specialiteEtudiant;
    }

    public String getMailPersonnel() {
        return mailPersonnel;
    }

    public void setMailPersonnel(String mailPersonnel) {
        this.mailPersonnel = mailPersonnel;
    }

    public String getMailEtudiant() {
        return mailEtudiant;
    }

    public void setMailEtudiant(String mailEtudiant) {
        this.mailEtudiant = mailEtudiant;
    }

    public String getAdresseEtudiant() {
        return adresseEtudiant;
    }

    public void setAdresseEtudiant(String adresseEtudiant) {
        this.adresseEtudiant = adresseEtudiant;
    }

    public String getNationaliteEtudiant() {
        return nationaliteEtudiant;
    }

    public void setNationaliteEtudiant(String nationaliteEtudiant) {
        this.nationaliteEtudiant = nationaliteEtudiant;
    }

    public int getTelephoneEtudiant() {
        return telephoneEtudiant;
    }

    public void setTelephoneEtudiant(int telephoneEtudiant) {
        this.telephoneEtudiant = telephoneEtudiant;
    }

    public String getIneEtudiant() {
        return ineEtudiant;
    }

    public void setIneEtudiant(String ineEtudiant) {
        this.ineEtudiant = ineEtudiant;
    }

    public List<Module> getModule() {
        return module;
    }

    public void setModule(List<Module> module) {
        this.module = module;
    }
}