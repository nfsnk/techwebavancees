package com.colysonko.scolaritemanager.etudiant;

import org.springframework.data.jpa.repository.JpaRepository;


import java.util.List;

public interface EtudiantRepository extends JpaRepository<Etudiant, Long> {
    List<Etudiant> findEtudiantsByModule(String module);

}