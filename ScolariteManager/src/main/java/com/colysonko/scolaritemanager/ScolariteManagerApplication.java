package com.colysonko.scolaritemanager;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ScolariteManagerApplication {

	public static void main(String[] args) {
		SpringApplication.run(ScolariteManagerApplication.class, args);
	}

}
