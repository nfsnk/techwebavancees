package com.colysonko.scolaritemanager.module;

import com.colysonko.scolaritemanager.etudiant.Etudiant;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "module")
public class Module {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_Module", nullable = false)
    private Long id;

    @Column(name="nom_Module", nullable = false)
    private String nomModule;

    @ManyToMany
    @JoinTable(name = "module_etudiant",
            joinColumns =@JoinColumn(name = "id_Module"),
            inverseJoinColumns =  @JoinColumn(name = "id_Etudiant"))
    private List<Etudiant> etudiant=new ArrayList<>();


    public Module() {
        super();
    }

    public Module(String nomModule) {
        super();
        this.nomModule = nomModule;
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id){
        this.id=id;
    }

    public String getNomModule() {
        return nomModule;
    }

    public void setNomModule(String nomModule) {
        this.nomModule = nomModule;
    }

    public List<Etudiant> getEtudiant() {
        return etudiant;
    }

    public void setEtudiant(List<Etudiant> etudiant) {
        this.etudiant = etudiant;
    }
}