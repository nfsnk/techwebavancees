package com.colysonko.scolaritemanager.module;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

@Path("modules")
public class ModuleRessources {
    private ModuleRepository moduleRepository;

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Module creerModule(Module m){return (moduleRepository.save(m));}

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("{idModule}")
    public Module getModule(@PathParam("idModule")  Long id){
        return (moduleRepository.getById(id));
    }

}
