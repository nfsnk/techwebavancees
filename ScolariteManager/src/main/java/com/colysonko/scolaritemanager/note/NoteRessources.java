package com.colysonko.scolaritemanager.note;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.Query;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;
import java.util.Optional;

@Path("notes")
public class NoteRessources {
    @Autowired
    private NoteRepository noteRepository;

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Note createNote(Note n){return (noteRepository.save(n));}

    @DELETE
    @Produces(MediaType.APPLICATION_JSON)
    public void deleteNoteByID(@QueryParam("idNote") Long id){noteRepository.deleteById(id);}

    @PATCH
    @Produces(MediaType.APPLICATION_JSON)
    public void updateNoteById(@QueryParam("idNote") Long id){}

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("{nomModule}")
    public List<Note> getListeNoteParModule(@PathParam("nomModule")  String nomModules){
        return  noteRepository.findNoteByModule_NomModule(nomModules);
    }

}
