package com.colysonko.scolaritemanager.note;

import com.colysonko.scolaritemanager.etudiant.Etudiant;
import com.colysonko.scolaritemanager.module.Module;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "note")
public class Note implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    /**C'est l'identifiant de la note*/
    private Long idNote;
    /**C'est la valeur de la note de l'étudiant*/
    private float valeurNote;

    @ManyToOne
    @JoinColumn(name = "id_Module",nullable=false)
    private Module module;

    @ManyToOne
    @JoinColumn(name = "id_Etudiant",nullable = false)
    private Etudiant etudiant;

    public Note() {
        super();
    }

    public Note(float valeurNote) {
        super();
        this.valeurNote = valeurNote;
    }



    public Long getIdNote() {
        return idNote;
    }

    public float getValeurNote() {
        return valeurNote;
    }
    public  void setValeurNote(float v){
        this.valeurNote= v;
    }

    public void setIdNote(Long idNote) {
        this.idNote = idNote;
    }

    public Module getModule() {
        return module;
    }

    public void setModule(Module module) {
        this.module = module;
    }

    public Etudiant getEtudiant() {
        return etudiant;
    }

    public void setEtudiant(Etudiant etudiant) {
        this.etudiant = etudiant;
    }
}