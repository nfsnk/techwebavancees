package com.colysonko.scolaritemanager.note;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import javax.ws.rs.QueryParam;
import java.util.List;

public interface NoteRepository extends JpaRepository<Note, Long> {
    List<Note> findByModule(Long idModule);

    @Query("select e.idEtudiant,e.prenomEtudiant,e.nomEtudiant,e.specialiteEtudiant,n.valeurNote from Note n, Module m, Etudiant e where n.idNote= m.id and m.nomModule=:nomModule")
    List<Note> findNoteByModule_NomModule(@QueryParam("nom_module") String nomModule);

}